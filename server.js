const express = require('express');
const app = express();

const router = require('./src-api/global-router');

// CORS middleware
var allowCrossDomain = function(req, res, next) {
  res.header('Access-Control-Allow-Origin', 'http://localhost:8080');
  res.header('Access-Control-Allow-Methods', 'GET,POST');
  res.header('Access-Control-Allow-Headers', 'Content-Type');

  next();
};

app.use(allowCrossDomain);

app.use('/api', router);

app.listen(3000, () => {
  console.log('listening on port 3000');
});
