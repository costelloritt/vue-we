import Vue from 'vue';
import Router from 'vue-router';
import FiveDayForecast from '../pages/five-day-forcast.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: '5 Day Forecast',
      component: FiveDayForecast
    }
  ]
});
