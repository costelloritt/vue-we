exports.parseHttpGetResponseToJson = (res, callback) => {
  const { statusCode } = res;

  // error handling
  let error;
  if (statusCode !== 200) {
    error = new Error(`Request Failed.\n Status Code: ${statusCode}`);
  }

  if (error) {
    console.error(error.message);
    res.resume();
    return;
  }

  res.setEncoding('utf-8');
  let rawData = '';

  res.on('data', (chunk) => {
    rawData += chunk;
  });

  res.on('end', () => {
    try {
      const parsedData = JSON.parse(rawData);
      callback(parsedData);
    } catch (e) {
      console.error(e.message);
    }
  }).on('error', (e) => {
    console.error(`Got error: ${e.message}`);
  });
};
