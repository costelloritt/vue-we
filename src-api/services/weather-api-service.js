'use strict';

const http = require('http');
const dataParser = require('./data-parser');

const weatherApiDomain = 'http://api.openweathermap.org/data/2.5';

exports.getFiveDayForecast = (city, countryCode, api, callback) => {
  http.get(`${weatherApiDomain}/forecast?q=${city},${countryCode}&mode=json&APPID=${api}`, (res) => {
    dataParser.parseHttpGetResponseToJson(res, callback);
  });
};

exports.getCurrentTemperature = (city, countryCode, api, callback) => {
  http.get(`${weatherApiDomain}/weather?q=${city},${countryCode}$mode=json&APPID=${api}`, (res) =>{
    dataParser.parseHttpGetResponseToJson(res, callback);
  });
};

exports.parseFiveDayForecast= (data, callback) => {

  let lastTimeOfDayRegex = /.*21:00:00/g;
  let forecastDays = [];
  let individualForecastDayData = [];
  // create 2D array. Top level array stores array of info about each day of
  // the week at each index
  for (let i = 0; i < data.list.length; i++) {
    individualForecastDayData.push({
      'day': data.list[i].dt_txt,
      'weatherInfo': data.list[i].main
    });
    if (lastTimeOfDayRegex.test(data.list[i].dt_txt)) {
      forecastDays.push(individualForecastDayData.slice());
      individualForecastDayData = [];
    }
  }
  // find the highest and lowest temperatures of the days listed
  let dailyHighLowTemps = [];
  let highLowTempObject = {
    'day': 0,
    'high': 0,
    'low': 1000
  };
  for (let i = 0; i < forecastDays.length; i++) {
    let tempHighLow = Object.assign({}, highLowTempObject);
    for (let j = 0; j < forecastDays[i].length; j++) {
      if (tempHighLow.high < forecastDays[i][j].weatherInfo.temp_max) {
        tempHighLow.high = forecastDays[i][j].weatherInfo.temp_max;
      }
      if (tempHighLow.low > forecastDays[i][j].weatherInfo.temp_min) {
        tempHighLow.low = forecastDays[i][j].weatherInfo.temp_min;
      }
    }
    tempHighLow.day = forecastDays[i][0].day;
    dailyHighLowTemps.push(Object.assign({}, tempHighLow));
  }
  // convert temps from kelvin to fahrenheit
  for (let i = 0; i < dailyHighLowTemps.length; i++) {
    dailyHighLowTemps[i].high = convertKelvinToFahrenheit(dailyHighLowTemps[i].high);
    dailyHighLowTemps[i].low = convertKelvinToFahrenheit(dailyHighLowTemps[i].low);
  }
  // remove time from the day property
  for (let i = 0; i < dailyHighLowTemps.length; i++) {
    let dateTimeString = dailyHighLowTemps[i].day;
    let spaceIndex = dateTimeString.indexOf(' ');
    dateTimeString = dateTimeString.substring(0, spaceIndex);
    dailyHighLowTemps[i].day = dateTimeString;
  }
  // get current day in same format as date information from weather api
  let currentDateTime = new Date();
  let currentYear = currentDateTime.getUTCFullYear();
  let currentMonth = currentDateTime.getUTCMonth() + 1;
  if (currentMonth < 10) {
    currentMonth = `0${currentMonth}`;
  }
  let currentDate = currentDateTime.getUTCDate();
  if (currentDate < 10) {
    currentDate = `0${currentDate}`;
  }
  let currentFullDate = `${currentYear}-${currentMonth}-${currentDate}`;
  // convert dates to days of the week
  while (currentFullDate !== dailyHighLowTemps[0].day) {
    currentDate = currentDate + 1;
    currentFullDate = `${currentYear}-${currentMonth}-${currentDate}`;
  }
  if (currentFullDate == dailyHighLowTemps[0].day) {
    for (let i = 0; i < dailyHighLowTemps.length; i++) {
      let dayOfWeek = currentDateTime.getUTCDay() + i;
      if (dayOfWeek > 6) {
        dayOfWeek = dayOfWeek - 7;
      }
      dailyHighLowTemps[i].day = daysOfWeek[dayOfWeek];
    }
  }
  // return day and high low temp object for response
  callback(dailyHighLowTemps);
};

exports.parseCurrentTemperature = (data, callback) => {
  let currentTemperature = {
    temp: convertKelvinToFahrenheit(data.main.temp)
  }
  callback(currentTemperature);
};

function convertKelvinToFahrenheit(temperature) {
  return Math.round(temperature * 9/5 - 459.67);
}

const daysOfWeek = [
  'Mon',
  'Tues',
  'Wed',
  'Thurs',
  'Fri',
  'Sat',
  'Sun'
];
