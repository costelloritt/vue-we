const routes = require('express').Router();

const weatherApiController = require('./controllers/weather-api-controller');

routes.get('/five-day-forecast/:city', weatherApiController.getFiveDayForecast);
routes.get('/current-weather/:city', weatherApiController.getCurrentTemperature);

module.exports = routes;
