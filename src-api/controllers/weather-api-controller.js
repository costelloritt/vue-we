const weatherService = require('../services/weather-api-service');
const api = require('../open-weather-map/api');

exports.getFiveDayForecast = (req, res) => {
  weatherService.getFiveDayForecast(req.params.city, 'us', api, (weatherData) => {
    weatherService.parseFiveDayForecast(weatherData, (responseObject) => {
      res.send(JSON.stringify(responseObject));
    });
  });
};

exports.getCurrentTemperature = (req, res) => {
  weatherService.getCurrentTemperature(req.params.city, 'us', api, (weatherData) => {
    weatherService.parseCurrentTemperature(weatherData, (responseObject) => {
      res.send(JSON.stringify(responseObject));
    });
  });
};
