VueJS - Five Day Forecast
==============

This web app is built with a VueJS front end and NodeJS/Express back end.

The API used to get weather data is https://openweathermap.org/.

Run Locally
--------------

Weather API Key
1. Create an account at https://openweathermap.org/ to receive an API key.
2. Add the directory/file "open-weather-map/api.js"
3. Export the key from this file.

VueJS
1. Run `npm install`
2. Run `npm run serve`

NodeJS
1. Run `npm start`

Browse to http://localhost:8080

Build
--------------
Currently no officially supported build steps.
